# Function Evaluation Pipeline

The Function Evaluation Pipeline combines, evaluates and visualizes results
of the Differential Expression and Sequence Annotation Pipelines.
Differentially expressed genes (DEGs) are merged with their corresponding
reference and novel annotations to enable filtering, profiling and enrichment
analysis of GO annotations, profiling and enrichment analysis of KEGG
pathways as well as exporting DEGs corresponding protein sequences for
further external analysis.

Enrichment analysis of GO functions currently requires intermediate manual
execution of Blast2GO. Instructions are provide while executing the Snakemake
workflow, which will be interrupted intentionally!


## Configuration and Execution

The pipeline is executed by calling Snakemake within the project folder
(working directory, configured paths are relative to here and all results and
intermediate data will be dropped here):

    snakemake -s path/to/pipeline/Snakefile --configfile snakemake.config.json --cores 4 -pk

A minimum configuration json file indicates significant DEGS as well as all
observed genes (such as as exported from DESeq2 in the Differential Expression
Pipeline) with a unique group/set id and two list of corresponding gene
annotation files (e.g. a set of reference and novels annotations from the
Sequence Annotation Pipeline), once in the *.annot and once in the *.tsv format.

    {
    "degs_significant" : {"group01" :
    "../de_results/deseq2/0.05/featureCounts/group01/target01/de-timeseries/design2/results_sig_ts_with_fc.csv"},
    "degs_complete" : {"group01" :
    "../de_results/deseq2/0.05/featureCounts/group01/target01/de-timeseries/design2/results_all_ts.csv"},
    "annotation_annots" : {"group01" :
        ["../anno_results/collection/target01ref/final.annot",
         "../anno_results/collection/target01nov/final.annot"]},
    "annotation_tsvs" : {"group01" :
        ["../anno_results/collection/target01ref/final.tsv",
         "../anno_results/collection/target01nov/final.tsv"]},
    }

For more configuration options (e.g. to also process KEGG annotations, set up
filtering or indicate time series), please refer to
[scripts_snakemake/configuration.snakefile](scripts_snakemake/configuration.snakefile)
and if necessary other related snakefiles.

Results may include (depending on configuration):

DEGs with annotations (maybe for different FC levels):  
`annotation/{group}/degs.annotated.tsv`,  
`annotation/{group}/degs{fc}.annotated.tsv` and/or  
`annotation/{group}/{group}.degs{fc}.maxfc.tsv`

DEGs filtered by GOs, descriptions or both:  
`filter/{group}/{filter_group}/go/degs{fc}.filtered.tsv`  
`filter/{group}/{filter_group}/description/degs.{description}.tsv`  
`filter/{group}/{filter_group}/merged/degs.labeled.tsv`

Filtered DEGs grouped into "issues":  
`filter/{group}/{filter_group}/merged/issues/{issue}.degs.tsv`  
`filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.png`

GO profiles (potentially including enrichment results):  
`visualization/go_profile/script/{group}/{group}.go.profile.all.pdf`

KEGG profiles (potentially including enrichment results):  
`kegg/{group}/degs.annotated_kegg.tsv`  
`visualization/kegg_profile/script/{group}/{group}.kegg.enriched.all.pdf`

DEG proteins for external analysis:  
`proteins/{group}/{group}.deg.proteins.fasta`


## Requirements

Requirements may vary with applied parameters (and thus executed Snakemake
rules), but the following lists all possibly used tools and libraries.

Software used in this workflow:
* Blast2GO
* GNU R
* Snakemake

R packages used in this workflow:
* clusterProfiler
* ggplot2
* GO.db
* goProfiles
* plyr
* reshape2
* RMySQL
* xlsx
