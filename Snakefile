import os
import datetime
import subprocess

from snakemake.utils import R

WORKFLOW_PATH = os.path.dirname(os.path.realpath(workflow.snakefile))
EXEC_TIME = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

### Configuration ###
include: WORKFLOW_PATH + "/scripts_snakemake/configuration.snakefile"

### Rules ###
include: WORKFLOW_PATH + "/scripts_snakemake/annotation.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/enrichment.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/filter.desc.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/filter.fc.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/filter.go.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/filter.merged.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/kegg.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/proteins.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reporting.versions.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/visualization.filter.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/visualization.profiles.snakefile"

rule all:
    input:
        # collect/merged annotations
        [expand("annotation/{group}/annotation.complete.annot", group=GROUPS)],
        [expand("annotation/{group}/annotation.complete.tsv", group=GROUPS)],
        ### annotation ###
        [expand("annotation/{group}/bar_go_id.png", group=GROUPS)],
        [expand("annotation/{group}/annotation.foreground.annot", group=GROUPS)],
        [expand("annotation/{group}/annotation.background.annot", group=GROUPS)],
        ### deg id lists ###
        [expand("annotation/{group}/degs.ids.all.txt", group=GROUPS)],
        [expand("annotation/{group}/degs.ids.go.txt", group=GROUPS)],
        ### FC filter ###
        [expand("annotation/{group}/degs{fc}.annotated.tsv", group=GROUPS, fc=FOLD_CHANGES)],
        [expand("annotation/{group}/{group}.degs{fc}.maxfc.xlsx", group=group, fc=FOLD_CHANGES)
                for group in GROUPS if group in TIMESERIES],
        ### GO, description & merged filter ###
        [expand("filter/{group}/{filter_group}/go/degs{fc}.filtered.tsv",
                filter_group=filter_group, group=group, fc=FOLD_CHANGES)
                for group in GROUPS for filter_group in FILTER_GROUPS[group] if "go" in FILTER_TYPES[filter_group]],
        [expand("filter/{group}/{filter_group}/description/degs.{description}.tsv",
                filter_group=filter_group, group=group, description=DESCRIPTIONS)
                for group in GROUPS for filter_group in FILTER_GROUPS[group] if "desc" in FILTER_TYPES[filter_group]],
        [expand("filter/{group}/{filter_group}/merged/{files}",
                filter_group=filter_group, group=group, files=["degs.labeled.tsv", "term.counts.tsv"])
                for group in GROUPS for filter_group in FILTER_GROUPS[group]  if "merge" in FILTER_TYPES[filter_group]],
        ### issue filter groups ###
        [expand("filter/{group}/{filter_group}/merged/issues/{issue}.degs.xlsx",
                group=group, filter_group=filter_group, issue=ISSUES[(group,filter_group)])
                for group in GROUPS for filter_group in FILTER_GROUPS[group] if (group,filter_group) in ISSUES],
        [expand("filter/{group}/{filter_group}/merged/{files}", group=group, filter_group=filter_group,
                files=["issues.fc.boxplots.png", "terms.fc.boxplots.pdf", "tag.issues.heatmaps"])
                for group in GROUPS for filter_group in FILTER_GROUPS[group] if (group,filter_group) in ISSUES],
        ### GO profiles ###
        [expand("visualization/go_profile/script/{group}/{group}.go.profile.all.pdf", group=GROUPS)],
        [expand("visualization/go_profile/goProfiles/{group}/barchart_level2.png", group=GROUPS)],
        [expand("visualization/go_profile/goProfiles/{group}/profiles_level2.txt", group=GROUPS)],
        ### DEG proteins for external analysis ###
        [expand("proteins/{group}/{group}.deg.proteins.fasta", group=group) for group in GROUPS if group in ANNO_PROT],
        ### KEGG analysis ###
        [expand("kegg/{group}/degs.annotated_kegg.tsv", group=group) for group in GROUPS if group in ANNO_KEGG],
        [expand("visualization/kegg_profile/script/{group}/{group}.kegg.enriched.all.pdf", group=group) for group in GROUPS if group in ANNO_KEGG],
        ### reporting ###
        "reports/" + EXEC_TIME + ".report.txt",
        "reports/" + EXEC_TIME + ".versions.txt",


### Snakemake logging ###
onsuccess:
    print("Workflow finished, no error")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.success.log")
    if MAIL_TO:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"FE evaluation of project '" + PROJECT + "': terminated successfully :-)\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.success.log " + MAIL_TO)

onerror:
    print("An error occurred")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.error.log")
    if MAIL_TO:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"FE evaluation of project '" + PROJECT + "': terminated unsuccessfully :-(\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.error.log " + MAIL_TO)


### Pipeline versioning ###

# get version of snakemake workflow (aka git tag + commit hash)
# NOTE: make sure to commit latest workflow to get an updated version number!
rule pipeline_version_git:
    output:
        temp("reports/pipeline.version")
    version:
        str(subprocess.check_output("cd " + WORKFLOW_PATH +
        " && git describe --tags --long", shell=True), 'utf-8').strip()
    run:
        file = open(output[0], "w")
        file.write(version)
        file.close()

# test report rule
rule pipeline_version_report:
    input:
        "reports/pipeline.version"
    output:
        "reports/" + EXEC_TIME + ".report.txt"
    shell:
        "echo \"FE Pipeline\" > {output} && echo \"Version:\" >> {output} && " \
        "cat {input} >> {output}"
