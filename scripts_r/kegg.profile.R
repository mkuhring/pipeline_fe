kegg.file <- snakemake@input$kegg
enr.file <- snakemake@input$enr
degs.file <- snakemake@input$degs
prefix <- snakemake@params$out_prefix
foldchange <- snakemake@params$foldchange

# get up/down regulation
degs.data <- read.table(file = degs.file, header = TRUE, sep = "\t", quote = "", stringsAsFactors = FALSE, na.strings = c("NA", ""))
degs.data <- degs.data[, c("Gene.ID", foldchange)]
degs.data$Regulation <- NA
degs.data$Regulation[degs.data[ , foldchange] < 1] <- "Down"
degs.data$Regulation[degs.data[ , foldchange] > 1] <- "Up"
degs.data[ , foldchange] <- NULL

# import KEGG pathway mappings
kegg.data <- read.table(file = kegg.file, header = TRUE, sep = "\t", quote = "",
                        stringsAsFactors = TRUE, na.strings = "", fill = TRUE)

# make sure gene 2 KO mappings are unique, to avoid over counting genes
kegg.data <- kegg.data[!duplicated(kegg.data[ ,c("Gene.ID", "KEGG.KO")]), ]

# integrate regulation
kegg.data <- merge(kegg.data, degs.data, by = "Gene.ID", all.x = TRUE)

# count genes per pathway
path.total <- table(kegg.data$KEGG.Pathway.Desc)
path.data <- data.frame(table(kegg.data[, c("KEGG.Pathway.Desc", "Regulation")]))
colnames(path.data) <- c("KEGG.Pathway", "Regulation", "Frequency")

# sort pathway factors by size
path.data <- within(path.data, KEGG.Pathway <-
                      factor(KEGG.Pathway, levels=sort(unique(KEGG.Pathway), decreasing = TRUE)))

# include enrichment data
enr.data <- read.table(file = enr.file, header = TRUE, sep = "\t", quote = "", stringsAsFactors = TRUE, comment.char = "")
path.data$Enriched <- path.data$KEGG.Pathway %in% enr.data$Description

# negate downregulated counts
idx <- path.data$Regulation == "Down"
path.data$Frequency[idx] <- -path.data$Frequency[idx]

require(ggplot2)
kegg.profile <- function(path.data, path.total, cutoff=0){{
  path.data <- path.data <- subset(path.data, (path.total > cutoff)[as.character(path.data$KEGG.Pathway)])
  freq.breaks = round(seq(min(path.data$Frequency), max(path.data$Frequency), length.out = 5))
  g <- ggplot() + coord_flip() +
    scale_x_discrete(name = "KEGG Pathway") +
    scale_y_continuous(name = "Frequency", breaks = freq.breaks) +
    facet_grid(. ~ Regulation, scales = "free_x", space = "free_x") +
    geom_bar(data = path.data, mapping = aes(x=KEGG.Pathway, y=Frequency), alpha = I(0.8), stat="identity")
  if (any(path.data$Enriched)){
    g <- g + geom_bar(data = path.data[path.data$Enriched, ], mapping = aes(x=KEGG.Pathway, y=Frequency, fill = Enriched), alpha = I(0.8), stat="identity")
  }
  return(g)
}}

ggsave(filename = paste0(prefix, "kegg.enriched.all.pdf"), plot = kegg.profile(path.data, path.total, 0))
ggsave(filename = paste0(prefix, "kegg.enriched.all.png"), plot = kegg.profile(path.data, path.total, 0))
if (max(abs(path.data$Frequency) > 1)){{
  ggsave(filename = paste0(prefix, "kegg.enriched.min2.pdf"), plot = kegg.profile(path.data, path.total, 1))
  ggsave(filename = paste0(prefix, "kegg.enriched.min2.png"), plot = kegg.profile(path.data, path.total, 1))
}}
if (max(abs(path.data$Frequency) > 2)){{
  ggsave(filename = paste0(prefix, "kegg.enriched.min3.pdf"), plot = kegg.profile(path.data, path.total, 2))
  ggsave(filename = paste0(prefix, "kegg.enriched.min3.png"), plot = kegg.profile(path.data, path.total, 2))
}}
if (max(abs(path.data$Frequency) > 4)){{
  ggsave(filename = paste0(prefix, "kegg.enriched.min5.pdf"), plot = kegg.profile(path.data, path.total, 4))
  ggsave(filename = paste0(prefix, "kegg.enriched.min5.png"), plot = kegg.profile(path.data, path.total, 4))
}}
