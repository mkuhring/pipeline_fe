### Filter DEGs based on GO-terms ###

# get all offsprings for GO filter terms
rule filter_go_get_offsprings:
    input:
        filter = "filter/{group}/{filter_group}/go/original/{filter}"
    output:
        filter = "filter/{group}/{filter_group}/go/extended/" + GO_OFFSPRINGS + "/{filter}"
    params:
        method = GO_OFFSPRINGS
    script:
        "../scripts_r/gofilter.offsprings.R"

# apply filter to DEGs
rule filter_go_filter_degs:
    input:
        degs = "annotation/{group}/degs.annotated.tsv",
        filter = "filter/{group}/{filter_group}/go/extended/" + GO_OFFSPRINGS + "/{filter}"
    output:
        degs = "filter/{group}/{filter_group}/go/degs/{filter}.tsv"
    run:
        # filter name
        filter_name = wildcards.filter.replace("_", " ")
        # parse filter GOs
        filter = set()
        names = dict()
        with open(input.filter, 'r') as f_in:
            for line in f_in:
                go_id = line.split("\t")[0]
                go_name = line.split("\t")[2].strip("\n")
                filter.add(go_id)
                names[go_id] = go_name
        # iterate DEGs, keep if GO matches
        with open(input.degs, 'r') as f_in:
            with open(output.degs, 'w') as f_out:
                f_out.write("GO.Filter.Text" + "\t" + "GO.Filter.ID" + "\t" + "GO.Filter.Name" + "\t" +  f_in.readline())
                for line in f_in:
                    gos = set(line.split("\t")[3].split(","))
                    matches = gos.intersection(filter)
                    if matches:
                        match_names = [names[match] for match in matches]
                        f_out.write(filter_name + "\t" + ",".join(matches) + "\t" + ";".join(match_names) + "\t" + line)

# collect all filtered DEGs
def filter_go_files(wc):
    filter_names, = glob_wildcards("filter/" + wc.group + "/" + wc.filter_group + "/go/original/{filter}")
    files = ["filter/" + wc.group + "/" + wc.filter_group + "/go/degs/" + filter + ".tsv" for filter in sorted(filter_names)]
    return(files)

rule filter_go_collection:
    input:
        filter = filter_go_files
    output:
        merged = "filter/{group}/{filter_group}/go/degs.filtered.tsv"
    shell:
        "head -1 {input.filter[0]} > {output.merged} && tail -n +2 -q {input.filter} >> {output.merged}"

# count all filtered DEGs
rule filter_go_counting:
    input:
        filter = filter_go_files
    output:
        counts = "filter/{group}/{filter_group}/go/filter.counts.tsv"
    shell: # TODO: count is one off due to header line
        "wc -l {input.filter} > {output.counts}"
