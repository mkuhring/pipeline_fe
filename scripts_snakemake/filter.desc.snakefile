### Filter/Mark DEGs based on Description terms ###

# apply filter on descriptions
rule filter_desc_apply:
    input:
        degs = "annotation/{group}/degs.annotated.tsv",
        filter = "filter/{group}/{filter_group}/description/filter.txt"
    output:
        degs = "filter/{group}/{filter_group}/description/degs.{description}.tsv"
    run:
        desc_header = {"main-desc" : "Sequence.Description",
                       "intp-desc" : "Supp.InterPro.Description",
                       "ref-desc"  : "Reference.Transcript.Header"}
        filter_header = {"main-desc" : "Filter.Seq.Desc",
                       "intp-desc" : "Filter.IP.Desc",
                       "ref-desc"  : "Filter.Ref.Desc"}
        desc_target = desc_header[wildcards.description]
        filter_out = filter_header[wildcards.description]

        need_header = True

        filter_names = set([line.strip("\n") for line in open(input.filter, 'r')])

        with open(output.degs, 'w') as f_out:

            for filter in filter_names:
                # filter = 'associated AND factor'
                if "OR" in filter:
                    filter_type = "OR" # match any
                    filter_terms = [term.strip().lower() for term in filter.split("OR")]
                elif filter.startswith("\"") and filter.endswith("\""):
                    filter_type = "FM" # full match
                    filter_terms = [filter.strip("\"").lower()]
                elif "AND" in filter:
                    filter_type = "AND" # match all, any order
                    filter_terms = [term.strip().lower() for term in filter.split("AND")]
                else: #  or several words
                    filter_type = "AND" # match all, any order
                    filter_terms = [term.strip().lower() for term in filter.split(" ")]
                print(filter_type + ":\t" + str(filter_terms))

                with open(input.degs, 'r') as f_in:
                    header = f_in.readline()
                    if need_header:
                        desc_index = header.strip("\n").split("\t").index(desc_target)
                        f_out.write(filter_out + "\t" + header)
                        need_header = False

                    for line in f_in:
                        description = line.strip("\n").split("\t")[desc_index].lower()

                        match = False
                        if filter_type is "OR":
                            match = any([term in description for term in filter_terms])
                        else: # filter_type in ["AND", "FM"]:
                            matches = [term in description for term in filter_terms]
                            match = all(matches) if matches else False
                        if match:
                            f_out.write(filter + "\t" + line)
