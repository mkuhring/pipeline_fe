### Filter DEGs based on Fold-Change ###

# filter DEGs based on fold-change threshold
rule filter_fc_apply:
    input:
        degs = "{folder}/degs.{suffix}.tsv"
    output:
        degs = "{folder}/degs{fc}.{suffix}.tsv"
    run:
        import math
        LOG_FC = math.log2(float(wildcards.fc))
        with open(input.degs, 'r') as f_in:
            with open(output.degs, 'w') as f_out:
                # get header
                header = f_in.readline()
                f_out.write(header)
                # find logFC columns
                colnames = header.strip("\n").split("\t")
                fc_log_idx = [i for i, s in enumerate(colnames) if 'log2FoldChange' in s]
                # iterate and test DEGs
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    fc_logs = [float(splits[i]) for i in fc_log_idx]
                    fc_tests = [abs(x) >= LOG_FC for x in fc_logs]
                    if any(fc_tests):
                        f_out.write(line)


# for timeseries, extract maximum fold-change per DEG
rule filter_fc_timeseries_max:
    input:
        degs = "annotation/{group}/degs{fc}.annotated.tsv"
    output:
        tsv = "annotation/{group}/{group}.degs{fc,([0-9]+([.][0-9]*)?)?}.maxfc.tsv"
    run:
        R(r'''
            # import degs
            degs <- read.table("{input.degs}", header = TRUE, sep = "\t", quote = "", stringsAsFactors = FALSE, na.strings = c("NA", ""))

            # get log index and timepoint pair
            log.idx <- grep(pattern = "^log2FoldChange_", colnames(degs))
            nor.idx <- grep(pattern = "^FoldChange_", colnames(degs))
            de.tp <- sub(pattern = "^log2FoldChange_", "", colnames(degs)[log.idx])

            # get max fold-change per gene
            max.idx <- max.col(abs(degs[, log.idx]))
            degs$Max.log2FoldChange <- NA
            degs$Max.FoldChange <- NA
            for (i in seq_along(max.idx)){{
              degs$Max.log2FoldChange[i] <- degs[i, log.idx[max.idx[i]]]
              degs$Max.FoldChange[i] <- degs[i, nor.idx[max.idx[i]]]
            }}
            degs$Max.Origin <- de.tp[max.idx]

            # select columns for output and sort
            degs.cols <- c("Gene.ID", "Sequence.Description", "Annotation.GO.ID", "Annotation.GO.Term",
                           "Max.log2FoldChange", "Max.FoldChange", "Max.Origin")
            degs.sub <- degs[, degs.cols]
            degs.sub <- degs.sub[order(degs.sub$Max.FoldChange, decreasing = T), ]

            # export
            write.table(degs.sub, "{output.tsv}", sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
        ''')
