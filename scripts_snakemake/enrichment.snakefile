### Prepare DEGs ###

# create simple DEG id list
rule deg_ids_all:
    input:
        anno = "annotation/{group}/degs.annotated.tsv" # sorted by padj
    output:
        ids = "annotation/{group}/degs.ids.all.txt"
    shell:
        "cut -f1 {input.anno} | tail -n +2 > {output.ids}"

# create simple DEG id list, DEGs with GO only
rule deg_ids_go:
    input:
        anno = "annotation/{group}/degs.annotated.tsv" # sorted by padj
    output:
        ids = "annotation/{group}/degs.ids.go.txt"
    shell:
        "grep \"GO:\" {input.anno} | cut -f1 > {output.ids}"


### Manual Enrichment with Blast2GO ###

# request manual Fisher's Exact Test via Blast2GO
rule enrich_blast2GO_fisher_manually:
    input:
        background = "annotation/{group}/annotation.background.annot",
        foreground = "annotation/{group}/degs.ids.go.txt"
    output:
        all = "enrichment/blast2go/{group}/fisher_fdr0.05_all.txt",
        spe = "enrichment/blast2go/{group}/fisher_fdr0.05_specific.txt"
    run:
        message = '''
### BLAST2GO ###
run the following steps manually in Blast2GO
1. import background annotations results:
   ''' + input.background + '''
   * File -> Load -> Load Annotations -> Load Annotations (.annot) (default parameters)
2. run the mapping
   ''' + input.foreground + '''
   * Analysis -> Enrichment Analysis (Fisher's Exact Text)
   non-default parameters
   * Two-sided = True
4. export results as txt for all and for specific terms only file:
   ''' + output.all + '''
   ''' + output.spe + '''
   * ... TODO ...
NOTE: In case of no significant results provide dummy result files:
   "touch ''' + output.all + '''"
   "touch ''' + output.spe + '''"
'''
        print(message)

# remove gene identifier from test to slim file
rule enrich_blast2GO_fisher_slim:
    input:
       tests = "enrichment/blast2go/{group}/fisher_fdr0.05_all.txt"
    output:
       tests = "enrichment/blast2go/{group}/fisher_fdr0.05_all.slim.txt"
    shell:
        "cut -f 1-10 {input.tests} > {output.tests}"


### KEGG Pathway Enrichment ###

# enrichment with clusterProfile (hypergeometrisch)
rule kegg_enrichment:
    input:
        fg = "kegg/{group}/kegg.foreground.txt",
        bg = "kegg/{group}/kegg.background.txt"
    output:
        enr = "kegg/{group}/kegg.path.enriched.txt"
    params:
        prefix = "kegg/{group}/"
    run:
        R(r'''
        require(clusterProfiler)
        require(ggplot2)
        annot.foreground <- read.table(file = "{input.fg}", header = TRUE, sep = "\t", fill = TRUE, na.strings = c(""), quote = "", stringsAsFactors = FALSE)
        annot.background <- read.table(file = "{input.bg}", header = TRUE, sep = "\t", fill = TRUE, na.strings = c(""), quote = "", stringsAsFactors = FALSE)

        # make sure gene 2 KO mappings are unique, to avoid over counting genes
        annot.foreground <- annot.foreground[!duplicated(annot.foreground[ ,c("Gene.ID", "KEGG.KO")]), ]
        annot.background <- annot.background[!duplicated(annot.background[ ,c("Gene.ID", "KEGG.KO")]), ]

        # select annotated genes and create mappings
        degs.idx <- which(!is.na(annot.foreground$KEGG.Pathway))
        degs <- annot.foreground$Gene.ID[degs.idx]
        universe.idx <- which(!is.na(annot.background$KEGG.Pathway))
        universe <- annot.background$Gene.ID[universe.idx]
        TERM2GENE <- data.frame(annot.background$KEGG.Pathway[universe.idx], annot.background$Gene.ID[universe.idx])
        TERM2NAME <-  data.frame(annot.background$KEGG.Pathway[universe.idx], annot.background$KEGG.Pathway.Desc[universe.idx])

        # enrich KEGG pathways
        x <- enricher(degs, universe = universe, TERM2GENE = TERM2GENE, TERM2NAME = TERM2NAME)
        write.table(as.data.frame(summary(x)), "{output.enr}", sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)

        # create and export plots
        bp <- barplot(x, x = "GeneRatio", colorBy = "p.adjust", showCategory = 100)
        dp <- dotplot(x, showCategory = 100)
        prefix = "{params.prefix}"
        ggsave(filename = paste0(prefix, "kegg.path.enriched.bp.pdf"), plot = bp)
        ggsave(filename = paste0(prefix, "kegg.path.enriched.bp.png"), plot = bp)
        ggsave(filename = paste0(prefix, "kegg.path.enriched.dp.pdf"), plot = dp)
        ggsave(filename = paste0(prefix, "kegg.path.enriched.dp.png"), plot = dp)

        # export genes and KOs with enriched pathways only
        degs.enr <- annot.foreground[annot.foreground$KEGG.Pathway %in% as.data.frame(summary(x))$ID, ]
        degs.enr <- degs.enr[order(degs.enr$KEGG.Pathway), ]
        write.table(degs.enr, paste0(prefix, "kegg.path.enriched.ko.txt"), sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
        ''')
