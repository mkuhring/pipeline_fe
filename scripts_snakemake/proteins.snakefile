### DEG Protein Sequences ###

# for further analysis, e.g. annotation via online services

# merge all proteins per group (e.g. reference and novels)
rule deg_proteins_merge_all:
    input:
        proteins = lambda wc: ANNO_PROT[wc.group]
    output:
        proteins = "proteins/{group}/{group}.all.proteins.fasta"
    shell:
        "cat {input.proteins} > {output.proteins}"

# merge corresponding ids per group
rule deg_proteins_merge_ids:
    input:
        ids = lambda wc: ANNO_IDS[wc.group]
    output:
        ids = "proteins/{group}/{group}.all.trans.gene.ids"
    shell:
        "cat {input.ids} > {output.ids}"

# extract DEG proteins
rule deg_proteins_collect:
    input:
        degs = lambda wc: DEGS_SIG[wc.group],
        proteins = "proteins/{group}/{group}.all.proteins.fasta",
        ids = "proteins/{group}/{group}.all.trans.gene.ids"
    output:
        proteins = "proteins/{group}/{group}.deg.proteins.fasta"
    run:
        # get DEG gene ids
        degs = set()
        with open(input.degs, 'r') as f_in:
            f_in.readline() # skip header
            for line in f_in:
                gene_id = line.split(",")[0].strip("\"").replace(':', '_')
                degs.add(gene_id)

        # get relevant trans
        trans = set()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                gene_id = splits[1].replace(':', '_')
                trans_id = splits[0].replace(':', '_')
                if gene_id in degs:
                    trans.add(trans_id)

        # iterate proteins
        with open(input.proteins, 'r') as f_in:
            with open(output.proteins, 'w') as f_out:
                is_deg = False
                for line in f_in:
                    if line.startswith(">"):
                        trans_id = line[1:].strip("\n").split(" ")[0].split("|")[0]
                        trans_id = re.sub(r"_ORF\d+$", "", trans_id)
                        is_deg = trans_id in trans
                    if is_deg:
                        f_out.write(line)
