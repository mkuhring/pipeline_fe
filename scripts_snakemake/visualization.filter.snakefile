### Visualize Filter Fold-Changes ###

# mainly intended for time series

# visualize FCs per filter issue via boxplots
rule vis_filter_fc_distribution_issues:
    input:
        issues = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".degs.tsv" for issue in ISSUES[(wc.group,wc.filter_group)]]
    output:
        box = "filter/{group}/{filter_group}/merged/issues.fc.boxplots.png"
    run:
        R(r'''
        issue.files <- strsplit("{input.issues}", " ")[[1]]
        print(issue.files)
        issue.name <- sapply(strsplit(basename(issue.files), ".", fixed = TRUE), `[[`, 1)
        issue.list <- lapply(issue.files, function(x){{read.table(file = x, header = TRUE, sep = "\t", quote = "", stringsAsFactors = FALSE, na.strings = c("NA", ""))}})

        lapply(issue.list, dim)

        has.hits <- sapply(issue.list, nrow) > 0
        issue.name <- issue.name[has.hits]
        issue.list <- issue.list[has.hits]

        for (i in seq_along(issue.list)){{
          issue.list[[i]]$Issue <- issue.name[i]
        }}

        issue.data <- do.call(rbind, issue.list)

        issue.fc.names <- grep(pattern = "log2FoldChange", x = colnames(issue.data), value = TRUE)

        library(reshape2)
        issue.wide <- issue.data[, c("Issue", issue.fc.names)]
        issue.long <- melt(issue.wide, id.vars = c("Issue"))
        colnames(issue.long) <- c("Issue", "Pair", "log2FoldChange")
        issue.long$Pair <- sub(pattern = "log2FoldChange_", replacement = "", issue.long$Pair)

        require(ggplot2)
        g <- ggplot(data = issue.long, mapping = aes(x = Issue, y = log2FoldChange, fill = Pair)) +
          geom_hline(aes(yintercept=0), color = "red") +
          geom_boxplot(alpha = 0.66) +
          theme(axis.text.x = element_text(angle = -45, hjust = 0))

        ggsave("{output.box}", g)
        ''')

# visualize FCs per filter term via boxplots
rule vis_filter_fc_distribution_terms:
    input:
        filter = "filter/{group}/{filter_group}/merged/degs.filter.tsv"
    output:
        box = "filter/{group}/{filter_group}/merged/terms.fc.boxplots.pdf"
    run:
        R(r'''
        term.file <- "{input.filter}"
        term.data <- read.table(file = term.file, header = TRUE, sep = "\t", quote = "", stringsAsFactors = FALSE, na.strings = c("NA", ""))
        Filter.Term <- apply(X = term.data[, 1:4], MARGIN = 1, FUN = function(x){{unique(x[!is.na(x)])}})
        term.data <- cbind(Filter.Term, term.data)

        term.fc.names <- grep(pattern = "log2FoldChange", x = colnames(term.data), value = TRUE)

        library(reshape2)
        term.wide <- term.data[, c("Filter.Term", term.fc.names)]
        term.long <- melt(term.wide, id.vars = c("Filter.Term"))
        colnames(term.long) <- c("Filter.Term", "Pair", "log2FoldChange")
        term.long$Pair <- sub(pattern = "log2FoldChange_", replacement = "", term.long$Pair)

        term.unique <- sort(unique(term.long$Filter.Term), decreasing = FALSE)
        term.long$Filter.Term <- factor(term.long$Filter.Term, levels = term.unique)
        term.splits <- split(term.unique, round(seq_along(term.unique)/10))

        require(ggplot2)
        pdf("{output.box}")
        for (terms in term.splits){{
          term.sub <- term.long[term.long$Filter.Term %in% terms, ]
          g <- ggplot(data = term.sub, mapping = aes(x = Filter.Term, y = log2FoldChange, fill = Pair)) +
            geom_hline(aes(yintercept=0), color = "red") +
            geom_boxplot(alpha = 0.66) +
            theme(axis.text.x = element_text(angle = -45, hjust = 0))
          print(g)
        }}
        dev.off()
        ''')

# visualize FCs per filter issue via heatmaps
# for all degs together and separately based on max FC up/down
rule vis_filter_fc_heatmap_collect:
    input:
        pdf = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.pdf" for issue in ISSUES[(wc.group,wc.filter_group)]],
        png = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.png" for issue in ISSUES[(wc.group,wc.filter_group)]],
        pdf_up = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.up.pdf" for issue in ISSUES[(wc.group,wc.filter_group)]],
        png_up = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.up.png" for issue in ISSUES[(wc.group,wc.filter_group)]],
        pdf_down = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.down.pdf" for issue in ISSUES[(wc.group,wc.filter_group)]],
        png_down = lambda wc: ["filter/" + wc.group + "/" + wc.filter_group + "/merged/issues/" + issue + ".fc.heatmap.down.png" for issue in ISSUES[(wc.group,wc.filter_group)]]
    output:
        tag = "filter/{group}/{filter_group}/merged/tag.issues.heatmaps"
    shell:
        "touch {output.tag}"

rule vis_filter_fc_heatmap:
    input:
        degs = "filter/{group}/{filter_group}/merged/issues/{issue}.degs.tsv"
    output:
        pdf = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.pdf",
        png = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.png"
    params:
        wfpath = WORKFLOW_PATH
    script:
        "../scripts_r/fc.heatmap.R"

rule vis_filter_fc_heatmap_split:
    input:
        degs = "filter/{group}/{filter_group}/merged/issues/{issue}.degs.tsv"
    output:
        pdf_up = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.up.pdf",
        png_up = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.up.png",
        pdf_down = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.down.pdf",
        png_down = "filter/{group}/{filter_group}/merged/issues/{issue}.fc.heatmap.down.png"
    params:
        wfpath = WORKFLOW_PATH
    script:
        "../scripts_r/fc.heatmap.split.R"
