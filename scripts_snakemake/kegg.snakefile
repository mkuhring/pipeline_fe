### Collect KEGG-Annotations ###

# merge input annotations to one file (e.g. reference and novels)
rule kegg_merge_annotation:
    input:
        kegg = lambda wc: ANNO_KEGG[wc.group]
    output:
        merged = "kegg/{group}/kegg.complete.txt"
    shell:
        "head -n 1 {input.kegg[0]} > {output.merged} && tail -q -n+2 {input.kegg} >> {output.merged}"


### Integrate DEGs and Annotations ###

# add KEGG-annotation to DEGs and foldchanges
# export is sorted by "padj" (DESeq2)
rule kegg_degs:
    input:
        kegg = "kegg/{group}/kegg.complete.txt",
        degs = lambda wc: DEGS_SIG[wc.group]
    output:
        kegg = "kegg/{group}/degs.annotated_kegg.tsv"
    run:
        R(r'''
            # import KEGG and DEGs
            file.kegg <- "{input.kegg}"
            file.degs <- "{input.degs}"

            table.kegg <- read.table(file.kegg, sep = "\t", header = TRUE, fill = TRUE,
                                     quote = "", stringsAsFactors = FALSE)
            table.degs <- read.table(file.degs, sep = ",", header = TRUE, stringsAsFactors = FALSE)
            colnames(table.degs)[1] <- "Gene.ID"

            # clean up DEG gene ids
            table.degs$Gene.ID <- gsub(pattern = ":", replacement = "_", x = table.degs$Gene.ID, fixed = TRUE)

            # remove unutilized data
            table.degs$baseMean <- NULL
            table.degs$lfcSE <- NULL
            table.degs$stat <- NULL

            # merge
            table.merge <- merge(table.kegg, table.degs, by="Gene.ID", all.y = TRUE)
            table.merge <- table.merge[order(table.merge$padj),]

            # export
            file.out <- "{output.kegg}"
            write.table(table.merge, file.out, sep = "\t", quote = FALSE, row.names = FALSE)
        ''')


### Annotation Foreground and Background ###

# create foreground annotations, i.e. select KEGG for significant DEGs
rule kegg_degs_foreground:
    input:
        kegg = "kegg/{group}/kegg.complete.txt",
        degs = lambda wc: DEGS_SIG[wc.group]
    output:
        kegg = "kegg/{group}/kegg.foreground.txt"
    run:
        genes = set()
        with open(input.degs, 'r') as f_in:
            next(f_in) # skip header
            genes = {line.split(",")[0].strip("\"").replace(":", "_") for line in f_in}

        with open(input.kegg, 'r') as f_in:
            with open(output.kegg, 'w') as f_out:
                f_out.write(f_in.readline())
                for line in f_in:
                    id = line.split("\t")[0]
                    if id in genes:
                        f_out.write(line)

# create background annotations, i.e. select KEGG-terms for all present genes
rule kegg_degs_background:
    input:
        kegg = "kegg/{group}/kegg.complete.txt",
        ids = "annotation/{group}/annotation.background.ids"
    output:
        kegg = "kegg/{group}/kegg.background.txt"
    run:
        genes = set()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                genes.add(line.strip("\n"))

        with open(input.kegg, 'r') as f_in:
            with open(output.kegg, 'w') as f_out:
                f_out.write(f_in.readline())
                for line in f_in:
                    id = line.split("\t")[0]
                    if id in genes:
                        f_out.write(line)
