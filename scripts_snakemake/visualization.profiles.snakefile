### Simple GO Profiles ###

# profiles via GO.db and ggplot2
rule vis_go_profile_script:
    input:
        annot = "annotation/{group}/annotation.foreground.annot",
        degs  = lambda wc: "annotation/" + wc.group + "/" + wc.group + ".degs.maxfc.tsv" if wc.group in TIMESERIES else "annotation/" + wc.group + "/degs.annotated.tsv",
        enr   = "enrichment/blast2go/{group}/fisher_fdr0.05_all.slim.txt"
    output:
        pdf_all  = "visualization/go_profile/script/{group}/{group}.go.profile.all.pdf",
        pdf_min5 = "visualization/go_profile/script/{group}/{group}.go.profile.min5.pdf"
    params:
        out_prefix = "visualization/go_profile/script/{group}/{group}.",
        foldchange = lambda wc: "Max.FoldChange" if wc.group in TIMESERIES else "FoldChange"
    script:
        "../scripts_r/go.profile.R"

# profiles via goProfiles
rule vis_go_profile_goProfiles:
    input:
        annot = "annotation/{group}/annotation.foreground.annot"
    output:
        log  = "visualization/go_profile/goProfiles/{group}/profiles.log",
        bar  = "visualization/go_profile/goProfiles/{group}/barchart_level2.png",
        data = "visualization/go_profile/goProfiles/{group}/profiles_level2.txt"
    run:
        R(r'''
            require(GO.db)
            require(goProfiles)

            sink("{output.log}")

            DEG.GOTermsFrame <- read.table("{input.annot}", sep = "\t", header = FALSE, quote = "", stringsAsFactor = FALSE)
            DEG.GOTermsFrame$V3 <- NULL
            colnames(DEG.GOTermsFrame) <- c("GeneID", "GOID")

            # drop non-GO annotations
            keep.idx <- which(grepl("^GO:", DEG.GOTermsFrame$GOID))
            cat("drop non-GO annotations:\n")
            print(DEG.GOTermsFrame[-keep.idx, ])
            DEG.GOTermsFrame <- DEG.GOTermsFrame[keep.idx, ]

            # get Ontology per term
            DEG.GOTermsFrame$Ontology <- NA
            for (i in seq_len(nrow(DEG.GOTermsFrame))){{
              goterm <- GOTERM[[DEG.GOTermsFrame$GOID[i]]]
              if (!is.null(goterm)){{
                DEG.GOTermsFrame$Ontology[i] <- Ontology(goterm)
              }}
            }}

            # drop unavailable GOs
            keep.idx <- which(!is.na(DEG.GOTermsFrame$Ontology))
            cat("drop GOs not available in GO.db:\n")
            print(DEG.GOTermsFrame[-keep.idx, ])
            DEG.GOTermsFrame <- DEG.GOTermsFrame[keep.idx, ]

            # calculate level 1 profile
            DEG.profile.l1 <- basicProfile(DEG.GOTermsFrame, level = 1, idType = "GOTermsFrame", empty.cats = FALSE)
            printProfiles(DEG.profile.l1)

            # calculate level 2 profile
            DEG.profile.l2 <- basicProfile(DEG.GOTermsFrame, level = 2, idType = "GOTermsFrame", empty.cats = FALSE)
            printProfiles(DEG.profile.l2)
            png(filename = "{output.bar}", width = 1000, height = 1500, pointsize = 30)
            plotProfiles(DEG.profile.l2, multiplePlots = TRUE)
            dev.off()

            sink()

            # export level 2 profiles
            profile.table <- rbind(data.frame(Ontology = "MF", DEG.profile.l2$MF),
                       data.frame(Ontology = "CC", DEG.profile.l2$CC),
                       data.frame(Ontology = "BP", DEG.profile.l2$BP))
            write.table(profile.table , "{output.data}", sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
        ''')


### KEGG Profiles ###

# pathway profiles of KEGG-annotated DEGs including enrichment
rule vis_kegg_profile:
    input:
        kegg = "kegg/{group}/kegg.foreground.txt",
        enr = "kegg/{group}/kegg.path.enriched.txt",
        degs  = lambda wc: "annotation/" + wc.group + "/" + wc.group + ".degs.maxfc.tsv" if wc.group in TIMESERIES else "annotation/" + wc.group + "/degs.annotated.tsv"
    output:
        pdf_all = "visualization/kegg_profile/script/{group}/{group}.kegg.enriched.all.pdf"
    params:
        out_prefix = "visualization/kegg_profile/script/{group}/{group}.",
        foldchange = lambda wc: "Max.FoldChange" if wc.group in TIMESERIES else "FoldChange"
    script:
        "../scripts_r/kegg.profile.R"
