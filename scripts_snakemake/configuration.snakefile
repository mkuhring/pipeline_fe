### Required Parameters ###

# differential expressed genes to analyse (dict: GroupID -> degs.tsv, DESeq2 so far)
DEGS_SIG = config["degs_significant"] # the significant DEGs only
DEGS_ALL = config["degs_complete"]    # all observed genes (one of the DESeq2 "all" files)

# list of gene annotation file, linear  (dict: GroupID -> [list of genes.annot])
# e.g. different file for reference and novel genes as provide by the Annotation pipeline
ANNO_ANNOT = config["annotation_annots"]

# list of gene annotation file, tabular  (dict: GroupID -> [list of genes.tsv])
# e.g. different file for reference and novel genes as provide by the Annotation pipeline
ANNO_TSV = config["annotation_tsvs"]


### Optional Parameters ###

# list of KEGG annotation file, tabular  (dict: GroupID -> [list of kegg.txt])
# e.g. different file for reference and novel genes as provide by the Annotation pipeline
ANNO_KEGG = config.get("annotation_kegg", dict())

# descriptions to filter
# put filter terms into: "filter\description\{group}\filter.txt"
DESCRIPTIONS = config.get("descriptions", ["main-desc", "intp-desc", "ref-desc"])

# other annotations (dict: GroupID -> [list of annotations.tsv])
# tab separated file, will be merged/attached via "Gene.ID" column
ANNO_PLUS = config.get("annotation_other", dict())

# reference transcript fasta and gtf.ids, to attach reference descriptions (dict: GroupID -> trans.fasta, genome.gtf.ids)
REF_FASTA = config.get("reference_fasta", dict())
REF_IDS   = config.get("reference_gtf.ids", dict())

# annotation proteins and gtf.ids.clean, to extract DEG proteins for further analysis (e.g. online annotation services)
ANNO_PROT = config.get("annotation_proteins", dict())
ANNO_IDS  = config.get("annotation_gtf.ids.clean", dict())

# fold-changes to filter
FOLD_CHANGES = config.get("fold_changes", [1.5, 1.8, 2])
if "" not in FOLD_CHANGES:
    FOLD_CHANGES.insert(0, "") # calculate results without filter

# indicate which groups are timeseries experiments
# for filtering and profiling based on maximal fold-changes
TIMESERIES = config.get("timeseries", [])

# dict of filter sets per group (dict: GroupID -> [list of filter_set])
FILTER_GROUPS = config.get("filter_groups", dict())

# types of filter to apply per filter set (dict: filter_set -> [list of filter types])
# "go": each filter set is expected to have go filter files in the folder:
#       "filter/{GroupID}/{filter_set}/go/original/"
# "desc": each filter set is expect to have description filter file (one term per line):
#         "filter/{GroupID}/{filter_set}/description/filter.txt"
# "merge": merge GO & Desc results for a filter set
FILTER_TYPES = config.get("filter_types", dict())

# method for retrieving GO offspring (required for GO filtering)
# possible value: "GO.db", "MySQL"
# MySQL is more up to date but doesn't work behind proxies!
GO_OFFSPRINGS = config.get("go_offspring_source", "GO.db")

# project description, only used for mails so far
PROJECT = config.get("project_name", "NONAME")

# reporting, i.e. snakemake log per mail
MAIL_TO = config.get("mail_to", "")
MAIL_FROM = config["mail_from"] if MAIL_TO else ""


### Conditional Parameters ###

# groups/samples to consider
GROUPS = DEGS_SIG.keys()

# get issues to group filter degs, if issue.tsv is available in merged filter
import os.path
ISSUES = dict()
for group in GROUPS:
    for filter_group in FILTER_GROUPS[group]:
        issues_file = "filter/" + group + "/" + filter_group + "/merged/issues.tsv"
        if os.path.isfile(issues_file):
            issues = dict()
            with open(issues_file, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    issues[splits[0]] = splits[1].split(",")
            ISSUES[(group, filter_group)] = issues


### Parameter Validation ###

# TODO: check if configuration is consistent
# i.e. groups have corresponding/required entries for each parameter
