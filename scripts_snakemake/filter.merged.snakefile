### Merge, Group and Count filtered DEGS ###

# merge filter files (Desc + GO Filter)
rule filter_merge:
    input:
        # TODO: make input (resp. filter) more optional
        f1 = "filter/{group}/{filter_group}/go/degs.filtered.tsv",
        f2 = "filter/{group}/{filter_group}/description/degs.main-desc.tsv",
        f3 = "filter/{group}/{filter_group}/description/degs.ref-desc.tsv",
        f4 = "filter/{group}/{filter_group}/description/degs.intp-desc.tsv",
    output:
        merged = "filter/{group}/{filter_group}/merged/degs.filter.tsv"
    run:
        R(r'''
            filter1 <- read.table("{input.f1}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "", na.strings = c(""))
            filter2 <- read.table("{input.f2}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "", na.strings = c(""))
            filter3 <- read.table("{input.f3}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "", na.strings = c(""))
            filter4 <- read.table("{input.f4}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "", na.strings = c(""))

            filter.terms <- sort(unique(c(filter1$GO.Filter.Text, filter2$Filter.Seq.Desc,
                              filter3$Filter.Ref.Desc, filter4$Filter.IP.Desc)))
            filter.list <- list()

            merge.list <- colnames(filter1)[which(colnames(filter1) == "Gene.ID") : length(colnames(filter1))]

            for (term in filter.terms){{
              idx1 <- filter1$GO.Filter.Text == term
              idx2 <- filter2$Filter.Seq.Desc == term
              idx3 <- filter3$Filter.Ref.Desc == term
              idx4 <- filter4$Filter.IP.Desc == term

              merge1 <- merge(filter1[idx1,], filter2[idx2,], by = merge.list, all = TRUE)
              merge2 <- merge(merge1, filter3[idx3,], by = merge.list, all = TRUE)
              merge3 <- merge(merge2, filter4[idx4,], by = merge.list, all = TRUE)
              filter.list[[term]] <- merge3
            }}

            final <- do.call(rbind, filter.list)

            front <- c("GO.Filter.Text", "Filter.Seq.Desc", "Filter.Ref.Desc", "Filter.IP.Desc")
            back <- colnames(final)[!colnames(final) %in% front]
            final <- final[, c(front, back)]

            write.table(final, "{output.merged}", sep = "\t", quote = FALSE,
                        col.names = TRUE, row.names = FALSE)
        ''')

# append DEGs without any filter hit
rule filter_merge_plus_rest:
    input:
        filter = "filter/{group}/{filter_group}/merged/degs.filter.tsv",
        normal = "annotation/{group}/degs.annotated.tsv"
    output:
        label = "filter/{group}/{filter_group}/merged/degs.labeled.tsv"
    run:
        R(r'''
            filter <- read.table("{input.filter}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "")
            normal <- read.table("{input.normal}", sep = "\t", header = TRUE, stringsAsFactors = FALSE, quote = "")

            f.degs <- unique(filter$Gene.ID)
            n.degs <- normal$Gene.ID
            n.idx <- !n.degs %in% f.degs

            to.add <- normal[n.idx, ]

            library(plyr)

            full <- rbind.fill(filter, to.add)

            write.table(full, "{output.label}", sep = "\t", quote = FALSE,
                        col.names = TRUE, row.names = FALSE)
        ''')

# count DEGs per filter term
rule filter_merge_term_count:
    input:
        degs = "filter/{group}/{filter_group}/merged/degs.labeled.tsv"
    output:
        counts = "filter/{group}/{filter_group}/merged/term.counts.tsv"
    run:
        filter_indices = [0, 1, 2, 3]
        counts = dict()
        with open(input.degs, 'r') as f_in:
            next(f_in)
            for line in f_in:
                splits = line.strip("\n").split("\t")
                for filter in [splits[i].rstrip() for i in filter_indices]:
                    if filter != "NA":
                        if filter in counts:
                            counts[filter] += 1
                        else:
                            counts[filter] = 1
                        break
        print(counts)
        with open(output.counts, 'w') as f_out:
            [f_out.write(term + "\t" + str(count) + "\n") for term, count in sorted(counts.items())]

# group by issue/objective
rule filter_merge_issue_groups:
    input:
        degs = "filter/{group}/{filter_group}/merged/degs.labeled.tsv"
    output:
        issue = "filter/{group}/{filter_group}/merged/issues/{issue}.degs.tsv",
        count = "filter/{group}/{filter_group}/merged/issues/{issue}.count.tsv"
    run:
        issue_filter = ISSUES[(wildcards.group, wildcards.filter_group)][wildcards.issue]
        filter_indices = [0, 1, 2, 3]
        gene2deg = dict()
        gene2filter = dict()
        with open(input.degs, 'r') as f_in:
            header = f_in.readline()
            for line in f_in:
                splits = line.strip("\n").split("\t")
                for filter in [splits[i].rstrip() for i in filter_indices]:
                    if filter in issue_filter:
                        gene = splits[4]
                        if gene not in gene2deg:
                            gene2deg[gene] = splits[5:]
                            gene2filter[gene] = set()
                        gene2filter[gene].add(filter)
        with open(output.issue, 'w') as f_out:
            f_out.write("Filter.Hits" + "\t" + "\t".join(header.split("\t")[4:]))
            [f_out.write(";".join(gene2filter[gene]) + "\t" + gene + "\t" + "\t".join(deg) + "\n") for gene, deg in sorted(gene2deg.items())]
        with open(output.count, 'w') as f_out:
            f_out.write(str(len(gene2deg)))

rule tsv_to_xls:
    input:
        tsv = "{file}.tsv"
    output:
        xlsx = "{file}.xlsx"
    run:
        R(r'''
            require(xlsx)
            data <- read.table("{input.tsv}", header = FALSE, sep = "\t", quote = "", stringsAsFactors = FALSE, na.strings = c("NA", ""))
            write.xlsx(x = data, file = "{output.xlsx}", col.names = FALSE, row.names = FALSE, showNA = TRUE)
        ''')

