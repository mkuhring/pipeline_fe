### Collect GO-Annotations ###

# TODO: Note about GO annotation pipeline

# merge input annotations to one file (e.g. reference and novels)
rule merge_annotation_annot:
    input:
        annos = lambda wc: ANNO_ANNOT[wc.group]
    output:
        merged = "annotation/{group}/annotation.complete.annot"
    shell:
        "cat {input.annos} > {output.merged}"

# merge input annotations to one file (e.g. reference and novels)
rule merge_annotation_tsv:
    input:
        annos = lambda wc: ANNO_TSV[wc.group]
    output:
        merged = "annotation/{group}/annotation.complete.tsv"
    shell:
        "cat {input.annos} > {output.merged}"


### Integrate DEGs and Annotations ###

# add GO-annotation to DEGs and foldchanges
# export is sorted by "padj" (DESeq2)
rule annot_degs_with_go:
    input:
        anno = "annotation/{group}/annotation.complete.tsv",
        degs = lambda wc: DEGS_SIG[wc.group]
    output:
        anno = "annotation/{group}/degs.annotated_go.tsv"
    run:
        R('''
            # import GO anno and DEGs
            file.anno <- "{input.anno}"
            file.degs <- "{input.degs}"

            table.anno <- read.table(file.anno, sep = "\\t", header = TRUE,
                                     quote = "", stringsAsFactors = FALSE)
            table.degs <- read.table(file.degs, sep = ",", header = TRUE, stringsAsFactors = FALSE)
            colnames(table.degs)[1] <- "Gene.ID"

            # clean up DEG gene ids
            table.degs$Gene.ID <- gsub(pattern = ":", replacement = "_", x = table.degs$Gene.ID, fixed = TRUE)

            # remove unutilized data
            table.degs$baseMean <- NULL
            table.degs$lfcSE <- NULL
            table.degs$stat <- NULL

            # merge
            table.merge <- merge(table.anno, table.degs, by="Gene.ID", all.y = TRUE)
            table.merge <- table.merge[order(table.merge$padj),]

            # export
            file.out <- "{output.anno}"
            write.table(table.merge, file.out, sep = "\\t", quote = FALSE, row.names = FALSE)
        ''')

# add additional annotations
rule annot_degs_with_other:
    input:
        degs = "annotation/{group}/degs.annotated_go.tsv",
        anno =  lambda wc: ANNO_PLUS[wc.group] if wc.group in ANNO_PLUS else []
    output:
        degs = "annotation/{group}/degs.annotated_other.tsv"
    run:
        if wildcards.group in ANNO_PLUS:
            R('''
            file.degs <- "{input.degs}"
            file.anno <- "{input.anno}"
            table.degs <- read.table(file.degs, sep = "\\t", header = TRUE,  quote = "", stringsAsFactors = FALSE)
            table.anno <- read.table(file.anno, sep = "\\t", header = TRUE,  quote = "", stringsAsFactors = FALSE)
            table.merge <- merge(table.degs, table.anno, by="Gene.ID", all = TRUE)
            table.merge <- table.merge[order(table.merge$padj),]
            file.out <- "{output.degs}"
            write.table(table.merge, file.out, sep = "\\t", quote = FALSE, row.names = FALSE)
            ''')
        else:
            shell("cp {input.degs} {output.degs}")

# append references transcriptions
rule annot_degs_with_reference:
    input:
        anno = "annotation/{group}/degs.annotated_other.tsv",
        fasta = lambda wc: REF_FASTA[wc.group],
        ids = lambda wc: REF_IDS[wc.group]
    output:
        anno = "annotation/{group}/degs.annotated_ref.tsv"
    run:
        # get fasta header
        trans2header = dict()
        with open(input.fasta, 'r') as f_in:
            for line in f_in:
                if line.startswith(">"):
                    trans_id = line[1:].strip("\n").split(" ")[0].split("|")[0]
                    trans_id = re.split('\.\d+$', trans_id)[0]
                    trans_id = trans_id.replace(':', '_')
                    trans2header[trans_id] = line[1:].strip("\n")
        # get gene 2 trans mapping
        gene2trans = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if splits[1].replace(':', '_') not in gene2trans:
                    gene2trans[splits[1].replace(':', '_')] = splits[0].replace(':', '_')
        # iterate DEGs, attach header
        with open(input.anno, 'r') as f_in:
            with open(output.anno, 'w') as f_out:
                f_out.write(f_in.readline().strip("\n") + "\t" + "Reference.Transcript.ID" + "\t" + "Reference.Transcript.Header" + "\n")
                for line in f_in:
                    gene_id = line.split("\t")[0].replace(':', '_')
                    if gene_id in gene2trans and gene2trans[gene_id] in trans2header:
                        line = line.strip("\n") + "\t" + gene2trans[gene_id]  + "\t" + trans2header[gene2trans[gene_id]]  + "\n"
                    else:
                        line = line.strip("\n") + "\t" + "" + "\t" + "" + "\n"
                    f_out.write(line)

# select whether reference descriptions should be include or GO annotations only
rule annot_degs_selections:
    input:
        anno = lambda wc: "annotation/" + wc.group + "/degs.annotated_ref.tsv" if wc.group in REF_FASTA \
                            else "annotation/" + wc.group + "/degs.annotated_other.tsv"
    output:
        anno = "annotation/{group}/degs.annotated.tsv"
    shell:
        "cp {input.anno} {output.anno}"


# bar charts to visualize annotation ratios (for timeseries only so far)
# (for protein description and GO ids)
rule annotation_visualization:
    input:
        anno = "annotation/{group}/degs.annotated.tsv"
    output:
        desc_anno = "annotation/{group}/bar_desc_anno.png",
        go_id = "annotation/{group}/bar_go_id.png"
    params:
        desc_ref = "annotation/{group}/bar_desc_ref.png" # optional, thus under params
    script:
        "../scripts_r/annotation.visualization.R"


### Annotation Foreground and Background ###

# create foreground annotations, i.e. select GO-terms for significant DEGs
rule annot_degs_foreground:
    input:
        anno = "annotation/{group}/annotation.complete.annot",
        degs = lambda wc: DEGS_SIG[wc.group]
    output:
        anno = "annotation/{group}/annotation.foreground.annot"
    run:
        go_lines = dict()
        with open(input.anno, 'r') as f_in:
            for line in f_in:
                id = line.split("\t")[0]
                if id not in go_lines:
                    go_lines[id] = list()
                go_lines[id].append(line)
        with open(input.degs, 'r') as f_in:
            with open(output.anno, 'w') as f_out:
                next(f_in)
                for line in f_in:
                    id = line.split(",")[0].strip("\"").replace(":", "_")
                    if id in go_lines:
                        f_out.writelines(go_lines[id])

# get ids for background annotations, i.e. all present genes
rule annot_degs_background_ids:
    input:
        degs = lambda wc: DEGS_ALL[wc.group]
    output:
        ids = "annotation/{group}/annotation.background.ids"
    run:
        with open(input.degs, 'r') as f_in:
            with  open(output.ids, 'w') as f_out:
                next(f_in)
                for line in f_in:
                    id = line.split(",")[0].strip("\"").replace(":", "_")
                    f_out.write(id + "\n")

# create background annotations, i.e. select GO-terms for all present genes
rule annot_degs_background:
    input:
        anno = "annotation/{group}/annotation.complete.annot",
        ids = "annotation/{group}/annotation.background.ids"
    output:
        anno = "annotation/{group}/annotation.background.annot"
    run:
        genes = set()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                genes.add(line.strip("\n"))

        with open(input.anno, 'r') as f_in:
            with open(output.anno, 'w') as f_out:
                for line in f_in:
                    id = line.split("\t")[0]
                    if id in genes:
                        f_out.write(line)
